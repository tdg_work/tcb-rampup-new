provider "aws" {
    access_key = "AKIAT6BYP3XOHMXLL5BL"
    secret_key = "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"
    region = "ap-south-1"
}

data "aws_vpc" "default" {
   default = true
  
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["ap-south-1a","ap-south-1b"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}

module "web_server_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"

  name        = "web-server"
  description = "Security group for web-server with HTTP ports open within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["10.10.0.0/16"]
}






resource "tls_private_key" "example_keypair" {

 algorithm = "RSA"

 rsa_bits = 2048

}

resource "aws_key_pair" "ssh-key" {
    key_name = "server-key"
    public_key = tls_private_key.example_keypair.public_key_openssh
}


data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners = ["amazon"]
    filter {
      name = "name"
      values = [var.image_name] 
    }
    filter {
      name = "virtualization-type"
      values = ["hvm"]
    }
}




module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "single-instance"

  ami                    = data.aws_ami.latest-amazon-linux-image.id
  instance_type          = "t2.micro"
  key_name               =  aws_key_pair.ssh-key.key_name
  monitoring             = true
  vpc_security_group_ids = [module.web_server_sg.security_group_id]
  subnet_id              = element(module.vpc.private_subnets, 0)

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
