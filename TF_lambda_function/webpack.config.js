const path = require('path');

module.exports = {
  entry: './index.js',
  output: {
    filename: 'example_lambda_function.js',
    path: path.resolve(__dirname, 'dist'),
    libraryTarget: 'commonjs2'
  },
  mode: 'production',
  target: 'node',
  optimization: {
    minimize: false
  }
};
