package test

import (
	"testing"

	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/packer"
	"github.com/stretchr/testify/assert"
)

func TestPackerHelloWorldExample(t *testing.T) {
	packerOptions := &packer.Options{
		// The path to where the Packer template is located
		Template: "../build.pkr.hcl",
	}

	amiID := packer.BuildAmi(t, packerOptions)
	// Verify that the image ID is not empty
	assert.NotEmpty(t, amiID, "Packer build did not return an image ID")

	// Clean up the AMI after we're done
	awsRegion := "ap-south-1"
	aws.DeleteAmi(t, awsRegion, amiID)
}
