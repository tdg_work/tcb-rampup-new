packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "example" {
  ami_name      = "my_ami"
  instance_type = "t2.micro"
  region        = "ap-south-1"
  source_ami    = "ami-0376ec8eacdf70aae"
  ssh_username  = "ec2-user"
  ssh_timeout   = "30m"

  provisioner "shell" {
    inline = [
      "sudo yum update",
      "sudo yum -y install nginx"
    ]
  }
}

build {
  name    = "my-packer"
  sources = ["source.amazon-ebs.example"]
}
